package com.spinsys.aws.web.factory;

import com.spinsys.aws.web.api.billing.BCMImpl;
import com.spinsys.aws.web.api.billing.BudgetImpl;
import com.spinsys.aws.web.api.billing.CostImpl;
import com.spinsys.aws.web.api.billing.PriceImpl;
import com.spinsys.aws.web.api.s3.PutImpl;
import com.spinsys.aws.web.api.s3.S3Impl;
import com.spinsys.aws.web.util.Parameters;
import com.spinsys.aws.web.util.Constants.ReportType;
import com.spinsys.aws.web.util.Constants.S3Action;

public class ImplFactory
{
	/**
	 * Return an instance of the AWS Billing and Cost Management API
	 * implementation class specified by the given report type.
	 * 
	 * @param reportType  the report type to access.
	 * @return  the corresponding API implementation class.
	 */
	public static BCMImpl getBillingImplementation( ReportType reportType, Parameters config )
	{
		BCMImpl impl;
		switch (reportType) {
			case BUDGET:
				impl = new BudgetImpl( config );
				break;
			case COST:
				impl = new CostImpl( config );
				break;
		case PRICE:
				impl = new PriceImpl( config );
				break;
			case USAGE:
				// TODO
				throw new RuntimeException( "TODO: unsupported report type " + reportType );
			default:
				impl = null;
		}

		return impl;
	}

	/**
	 * Return an instance of the Amazon Simple Storage Service (S3) API
	 * implementation class specified by the given action type.
	 * 
	 * @param actionType  the S3 action to perform.
	 * @return  the corresponding API implementation class.
	 */
	public static S3Impl getS3Implementation( S3Action actionType, Parameters config )
	{
		S3Impl impl;
		switch (actionType) {
			case DELETE:
				// TODO
			case GET:
				// TODO
			case LIST:
				// TODO
				throw new RuntimeException( "TODO: unsupported action type " + actionType );
			case PUT:
				impl = new PutImpl( config );
				break;
			default:
				impl = null;
		}

		return impl;
	}
}
