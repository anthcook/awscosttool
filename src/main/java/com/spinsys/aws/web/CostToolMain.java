package com.spinsys.aws.web;

import com.spinsys.aws.web.api.billing.BCMImpl;
import com.spinsys.aws.web.api.s3.S3Impl;
import com.spinsys.aws.web.factory.ImplFactory;
import com.spinsys.aws.web.util.Utils;
import com.spinsys.aws.web.util.Constants.S3Action;
import com.spinsys.aws.web.util.Constants;
import com.spinsys.aws.web.util.Parameters;
import java.io.File;
import java.io.FileOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class CostToolMain
{
	public static void main( String[] args )
	{
		try {
			Parameters config = new Parameters( args );
			if (!config.hasCredentials() || !config.hasRequiredParameters()) {
				System.out.println( config.getUsage() );
			}
			else {
				BCMImpl bcm = ImplFactory.getBillingImplementation( config.getReportType(), config );
				String result = bcm.execute();
				if (config.getResponseFile() != null) {
					writeResponse( result, config );
				}
			}
		}
		catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error( "Error running client.", e );
			}
		}
	}
	
	/*
	 * Writes the give response to one of the following:
	 * 1. The output file path specified in parameters if not a directory; or
	 *
	 * 2. The output directory specified in parameters with file name same as
	 * the input file but with '.rsp' suffix (e.g. --file=myinput.json ->
	 * myinput.rsp); or
	 *
	 * 3. The output directory specified in parameters with file name same as
	 * the specified document type with suffix '.rsp' if input file not
	 * specified.
	 */
	private static File writeResponse( String response, Parameters config )
	{
		File responseFile = config.getResponseFile();
		Constants.ReportType reportType = config.getReportType();

		File outFile;
		if (responseFile.isDirectory()) {
			outFile = new File( responseFile, String.format("%s.rpt", reportType.toString()) );
		}
		else {
			outFile = responseFile;
		}
		
		try (FileOutputStream out = new FileOutputStream(outFile)) {
			out.write( Utils.beautify(response).getBytes() );
			out.flush();
			System.out.println( String.format("%s report written to file %s.", reportType.toString(), outFile.toString()) );
		}
		catch( Exception e ) {
			outFile = null;
			if (LOG.isErrorEnabled()) {
				LOG.error( String.format("Error creating response file %s.", outFile) );
			}
		}
		
		if (config.doUploadToS3()) {
			if (config.getS3ObjectName() == null) {
				config.setS3ObjectName( outFile.getName() );
			}
			config.setResponseFile( outFile );
			S3Impl s3 = ImplFactory.getS3Implementation( S3Action.PUT, config );
			String result = s3.execute();
			System.out.println( String.format("Received S3 action result:\n%s", Utils.beautify(result)) );
			outFile.deleteOnExit();
		}

		return outFile;
	}

	
	/*
	static {
		String path = CostToolMain.class.getClassLoader().
				getResource( "logging.properties" ).getFile();
		System.setProperty( "java.util.logging.config.file", path );
		System.setProperty( "org.apache.commons.logging.Log", "org.apache.commons.logging.impl.Jdk14Logger" );
	}
	*/
	
	private static final Log LOG = LogFactory.getLog( CostToolMain.class );
}
