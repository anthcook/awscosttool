package com.spinsys.aws.web.api.s3;

import java.io.File;
import java.util.Map;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.internal.SSEResultBase;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.spinsys.aws.web.api.AWSImpl;
import com.spinsys.aws.web.util.Parameters;
import com.veetechis.lib.text.JSONWriter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Simple Storage Service (S3) API implementation to upload files to a storage
 * bucket.
 */
public class PutImpl
extends S3Impl
{
	/**
	 * Creates an instance of UploadImpl.
	 */
	public PutImpl( Parameters configuration )
	{
		super( configuration );
	}

	
	/**
	 * Uploads a file to a storage location on S3.
	 * 
	 * @return  AWS product pricing information.
	 * @see  AWSImpl#execute()
	 */
	@Override
	public String execute()
	throws AmazonClientException, AmazonServiceException
	{
		AmazonS3 client = AmazonS3ClientBuilder.standard().
			withCredentials( getCredentialsProvider() ).
			withRegion( Regions.fromName(getConfiguration().getRegion()) ).
			build();

		logInfo( LOG, "Getting started with Amazon S3 (upload)..." );

		String bucketName = getConfiguration().getS3BucketName();
		String objectName = getConfiguration().getS3ObjectName();
		File uploadFile = getConfiguration().getResponseFile();
		PutObjectRequest request = new PutObjectRequest( bucketName, objectName, uploadFile );
		PutObjectResult result = client.putObject( request );
		String response = format( result, getConfiguration().getDataFormat() );

		logInfo( LOG, String.format("Received upload response:\n%s", response) );

		return response;
	}

	/**
	 * Formats and returns the given Simple Storage Service (S3) API call
	 * result in the specified data format.
	 * 
	 * @return  the result of the API call in the specified format.
	 */
	@Override
	public String format( SSEResultBase result, DataFormat format )
	{
		JSONWriter writer = new JSONWriter();
		writer.startDocument();
		if (result instanceof PutObjectResult) {
			PutObjectResult por = (PutObjectResult) result;
			writer.addQuotedField( "ContentMD5", por.getContentMd5() );
			writer.addQuotedField( "ETag", por.getETag() );
			writer.addQuotedField( "ExpirationTime", DATETIME_FORMAT.format(por.getExpirationTime()) );
			writer.addQuotedField( "ExpirationTimeRuleID", por.getExpirationTimeRuleId() );
			writer.addUnquotedField( "RequesterCharged", String.valueOf(por.isRequesterCharged()) );
			writer.addQuotedField( "VersionID", por.getVersionId() );
			formatObjectMetadata( por.getMetadata(), writer );
		}
		else {
			writer.addQuotedField( "Error", String.format("Unsupported result type - %s", result.getClass().getName()) );
		}
		writer.endDocument();

		return writer.toString();
	}

	protected void formatObjectMetadata( ObjectMetadata metadata, JSONWriter writer )
	{
		if (metadata != null) {
			writer.startObject( "Metadata" );
			writer.addQuotedField( "ArchiveStatus", metadata.getArchiveStatus() );
			if (metadata.getBucketKeyEnabled() != null) {
				writer.addUnquotedField( "BucketKeyEnabled", String.valueOf(metadata.getBucketKeyEnabled()) );
			}
			writer.addQuotedField( "CacheControl", metadata.getCacheControl() );
			writer.addQuotedField( "ContentDisposition", metadata.getContentDisposition() );
			writer.addQuotedField( "ContentEncoding", metadata.getContentEncoding() );
			writer.addUnquotedField( "ContentLength", String.valueOf(metadata.getContentLength()) );
			writer.addQuotedField( "ContentType", metadata.getContentType() );
			if (metadata.getHttpExpiresDate() != null) {
				writer.addQuotedField( "HttpExpiresDate", DATE_FORMAT.format(metadata.getHttpExpiresDate()) );
			}
			writer.addUnquotedField( "InstanceLength", String.valueOf(metadata.getInstanceLength()) );
			if (metadata.getLastModified() != null) {
				writer.addQuotedField( "LastModified", DATE_FORMAT.format(metadata.getLastModified()) );
			}
			writer.addQuotedField( "ObjectLockLegalHoldStatus", metadata.getObjectLockLegalHoldStatus() );
			writer.addQuotedField( "ObjectLockMode", metadata.getObjectLockMode() );
			if (metadata.getObjectLockRetainUntilDate() != null) {
				writer.addQuotedField( "ObjectLockRetailUntilDate", DATE_FORMAT.format(metadata.getObjectLockRetainUntilDate()) );
			}
			if (metadata.getOngoingRestore() != null) {
				writer.addUnquotedField( "OngoingRestore", String.valueOf(metadata.getOngoingRestore()) );
			}
			if (metadata.getPartCount() != null) {
				writer.addUnquotedField( "PartCount", String.valueOf(metadata.getPartCount()) );
			}
			writer.addQuotedField( "ReplicationStatus", metadata.getReplicationStatus() );
			if (metadata.getRestoreExpirationTime() != null) {
				writer.addQuotedField( "RestoreExpirationTime", DATETIME_FORMAT.format(metadata.getRestoreExpirationTime()) );
			}
			writer.addQuotedField( "SSEAlgorithm", metadata.getSSEAlgorithm() );
			writer.addQuotedField( "SSEAwsKmsEncryptionContext", metadata.getSSEAwsKmsEncryptionContext() );
			writer.addQuotedField( "SSEAwsKmsKeyID", metadata.getSSEAwsKmsKeyId() );
			writer.addQuotedField( "SSECustomerAlgorithm", metadata.getSSECustomerAlgorithm() );
			writer.addQuotedField( "SSECustomerKeyMD5", metadata.getSSECustomerKeyMd5() );
			writer.addQuotedField( "StorageClass", metadata.getStorageClass() );
			formatContentRange( metadata.getContentRange(), writer );
			formatRawMetadata( metadata.getRawMetadata(), writer );
			formatUserMetadata( metadata.getUserMetadata(), writer );
			writer.endObject();
		}
	}

	protected void formatContentRange( Long[] values, JSONWriter writer )
	{
		if (values != null) {
			writer.startList( "ContentRange" );
			for (int i = 0; i < values.length; i++) {
				writer.addElement( "\"" + values[i] + "\"" );
			}
			writer.endList();
		}
	}

	protected void formatRawMetadata( Map<String,Object> rmetadata, JSONWriter writer )
	{
		if (rmetadata != null) {
			writer.startObject( "RawMetadata" );
			for (String key: rmetadata.keySet()) {
				writer.addQuotedField( key, String.valueOf(rmetadata.get(key)) );
			}
			writer.endObject();
		}
	}

	protected void formatUserMetadata( Map<String,String> umetadata, JSONWriter writer )
	{
		if (umetadata != null) {
			writer.startObject( "UserMetadata" );
			for (String key: umetadata.keySet()) {
				writer.addQuotedField( key, String.valueOf(umetadata.get(key)) );
			}
			writer.endObject();
		}
	}


	private static final Log LOG = LogFactory.getLog( PutImpl.class );
}
