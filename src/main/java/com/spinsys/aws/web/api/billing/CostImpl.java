package com.spinsys.aws.web.api.billing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.AmazonWebServiceResult;
import com.amazonaws.ResponseMetadata;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.costexplorer.AWSCostExplorer;
import com.amazonaws.services.costexplorer.AWSCostExplorerClientBuilder;
import com.amazonaws.services.costexplorer.model.BillExpirationException;
import com.amazonaws.services.costexplorer.model.DataUnavailableException;
import com.amazonaws.services.costexplorer.model.DateInterval;
import com.amazonaws.services.costexplorer.model.DimensionValuesWithAttributes;
import com.amazonaws.services.costexplorer.model.GetCostAndUsageRequest;
import com.amazonaws.services.costexplorer.model.GetCostAndUsageResult;
import com.amazonaws.services.costexplorer.model.Granularity;
import com.amazonaws.services.costexplorer.model.Group;
import com.amazonaws.services.costexplorer.model.GroupDefinition;
import com.amazonaws.services.costexplorer.model.LimitExceededException;
import com.amazonaws.services.costexplorer.model.MetricValue;
import com.amazonaws.services.costexplorer.model.ResultByTime;
import com.spinsys.aws.web.api.AWSImpl;
import com.spinsys.aws.web.util.Parameters;
import com.veetechis.lib.text.JSONWriter;
import com.veetechis.lib.util.KeyValuePair;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Billing and Cost Management API implementation that provides access to
 * cost metrics for the account.
 */
public class CostImpl
extends BCMImpl
{
	/**
	 * Creates an instance of CostImpl.
	 */
	public CostImpl( Parameters configuration )
	{
		super( configuration );
	}

	
	/**
	 * Queries and returns cost metrics for the account. If a specific cost
	 * metric isn't specified by name in configuration then returns all cost
	 * metrics.
	 * 
	 * @return  cost metrics for the account.
	 * @see  AWSImpl#execute()
	 */
	@Override
	public String execute()
	throws AmazonClientException, AmazonServiceException
	{
		AWSCostExplorer costs = AWSCostExplorerClientBuilder.standard().
			withCredentials( getCredentialsProvider() ).
			withRegion( Regions.fromName(getConfiguration().getRegion()) ).
			build();

		logInfo( LOG, "Getting started with AWS Cost Explorer..." );

		String costInfo;
		String costMetrics = getConfiguration().getReportType().getSubtype();
		if (costMetrics == null) {
			costMetrics = "AmortizedCost,BlendedCost,NetAmortizedCost,NetUnblendedCost,NormalizedUsageAmount,UnblendedCost,UsageQuantity";
		}
		String start, end;
		if (getConfiguration().hasTimePeriod()) {
			start = DATE_FORMAT.format( getConfiguration().getTimePeriodStart() );
			end = DATE_FORMAT.format( getConfiguration().getTimePeriodEnd() );
		}
		else {
			Calendar cal = Calendar.getInstance();
			end = DATE_FORMAT.format( cal.getTime() );
			cal.set( Calendar.DATE, 1 );
			start = DATE_FORMAT.format( cal.getTime() );
		}
		List<GroupDefinition> groupByList = new ArrayList<GroupDefinition>();
		if (getConfiguration().hasCostGroup()) {
			KeyValuePair[] grouping = getConfiguration().getCostGrouping().toArray();
			if (grouping != null) {
				for (int i = 0; i < grouping.length; i++) {
					groupByList.add(
						new GroupDefinition().
							withType( grouping[i].getKey() ).
							withKey( grouping[i].getValue() )
					);
				}
			}
		}
		GetCostAndUsageRequest request =
			new GetCostAndUsageRequest().
				withGranularity( Granularity.MONTHLY ).
				withMetrics( Arrays.asList(costMetrics.split("\\,")) ).
				withGroupBy( groupByList ).
				withTimePeriod( new DateInterval().withEnd(end).withStart(start) );

		GetCostAndUsageResult result = null;
		try {
			result = costs.getCostAndUsage( request );
			costInfo = format( result, getConfiguration().getDataFormat() );
		}
		catch (LimitExceededException | BillExpirationException | DataUnavailableException e) {
			costInfo = "{\"Error\": \"" + e.getMessage() + "\"}";
		}

		logInfo( LOG, String.format("Received cost metrics:\n%s", costInfo) );

		return costInfo;
	}

	/**
	 * Formats and returns the given Billing and Cost Management API call
	 * result in the specified data format.
	 * 
	 * @return  the result of the API call in the specified format.
	 */
	@Override
	public String format( AmazonWebServiceResult<ResponseMetadata> result, DataFormat format )
	{
		JSONWriter writer = new JSONWriter();
		writer.startDocument();
		if (result instanceof GetCostAndUsageResult) {
			formatDimensionValueAttributes( ((GetCostAndUsageResult)result).getDimensionValueAttributes(), writer );
			formatCostAndUsage(
				((GetCostAndUsageResult)result).getGroupDefinitions(),
				((GetCostAndUsageResult)result).getResultsByTime(),
				((GetCostAndUsageResult)result).getNextPageToken(),
				writer );
		}
		else {
			writer.addQuotedField( "Error", String.format("Unsupported result type - %s", result.getClass().getName()) );
		}
		writer.endDocument();

		return writer.toString();
	}

	protected void formatCostAndUsage( List<GroupDefinition> groups, List<ResultByTime> resultsByTime, String token, JSONWriter writer )
	{
		formatGroupDefinitions( groups, writer );
		formatResultsByTime( resultsByTime, writer, (token != null) );
		if (token != null) {
			writer.addQuotedField( "NextPageToken", token );
		}
	}

	protected void formatGroupDefinitions( List<GroupDefinition> groups, JSONWriter writer )
	{
		writer.startList( "GroupDefinitions" );
		if (groups != null) {
			for (GroupDefinition def : groups) {
				writer.startObject();
				writer.addQuotedField( "Key", def.getKey() );
				writer.addQuotedField( "Type", def.getType() );
				writer.endObject();
			}
		}
		writer.endList();
	}

	protected void formatResultsByTime( List<ResultByTime> results, JSONWriter writer, boolean hasNextBlock )
	{
		writer.startList( "ResultsByTime" );
		if (results != null) {
			for (ResultByTime rbt: results) {
				writer.startObject();
				writer.addUnquotedField( "Estimated", String.valueOf(rbt.isEstimated()) );
				formatResultGroups( rbt.getGroups(), writer );
				formatTimePeriod( rbt.getTimePeriod(), writer );
				formatMetrics( "Total", rbt.getTotal(), writer, false );
				writer.endObject();
			}
		}
		writer.endList();
	}

	protected void formatResultGroups( List<Group> groups, JSONWriter writer )
	{
		writer.startList( "Groups" );
		if (groups != null) {
			for (Group grp: groups) {
				writer.startObject();
				formatKeys( grp.getKeys(), writer );
				formatMetrics( "Metrics", grp.getMetrics(), writer, false );
				writer.endObject();
			}
		}
		writer.endList();
	}

	protected void formatKeys( List<String> keys, JSONWriter writer )
	{
		writer.startList( "Keys" );
		if (keys != null) {
			for (String key: keys) {
				writer.addElement( "\"" + key + "\"" );
			}
		}
		writer.endList();
	}

	protected void formatMetrics( String title, Map<String,MetricValue> metrics, JSONWriter writer, boolean hasNextBlock )
	{
		writer.startObject( title );
		if (metrics != null) {
			for (String key: metrics.keySet()) {
				writer.startObject( key );
				writer.addQuotedField( "Amount", metrics.get(key).getAmount() );
				writer.addQuotedField( "Unit", metrics.get(key).getUnit() );
				writer.endObject();
			}
		}
		writer.endObject();
	}

	protected void formatTimePeriod( DateInterval period, JSONWriter writer )
	{
		writer.startObject( "TimePeriod" );
		writer.addQuotedField( "End", period.getEnd() );
		writer.addQuotedField( "Start", period.getStart() );
		writer.endObject();
	}

	protected void formatDimensionValueAttributes( List<DimensionValuesWithAttributes> dimValues, JSONWriter writer )
	{
		writer.startList( "DimensionValueAttributes" );
		if (dimValues != null) {
			for (DimensionValuesWithAttributes valAttrs: dimValues) {
				writer.startObject();
				Map<String,String> attrs = valAttrs.getAttributes();
				if (attrs != null) {
					writer.startObject( "Attributes" );
					for (String attrib: attrs.keySet()) {
						writer.addQuotedField( attrib, attrs.get(attrib) );
					}
					writer.endObject();
				}
				writer.addQuotedField( "Value", valAttrs.getValue() );
				writer.endObject();
			}
		}
		writer.endList();
	}


	private static final Log LOG = LogFactory.getLog( CostImpl.class );
}
