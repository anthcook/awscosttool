package com.spinsys.aws.web.api.billing;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.AmazonWebServiceResult;
import com.amazonaws.ResponseMetadata;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.pricing.AWSPricing;
import com.amazonaws.services.pricing.AWSPricingClientBuilder;
import com.amazonaws.services.pricing.model.Filter;
import com.amazonaws.services.pricing.model.FilterType;
import com.amazonaws.services.pricing.model.GetProductsRequest;
import com.amazonaws.services.pricing.model.GetProductsResult;
import com.spinsys.aws.web.api.AWSImpl;
import com.spinsys.aws.web.util.Parameters;
import com.veetechis.lib.text.JSONWriter;
import com.veetechis.lib.util.KeyValuePair;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Billing and Cost Management API implementation that provides access to
 * AWS product pricing.
 */
public class PriceImpl
extends BCMImpl
{
	/**
	 * Creates an instance of PriceImpl.
	 */
	public PriceImpl( Parameters configuration )
	{
		super( configuration );
	}

	
	/**
	 * Queries and returns pricing information about an AWS product.
	 * If a specific product isn't specified by name in configuration then
	 * returns pricing for all currently available products.
	 * 
	 * @return  AWS product pricing information.
	 * @see  AWSImpl#execute()
	 */
	@Override
	public String execute()
	throws AmazonClientException, AmazonServiceException
	{
		AWSPricing pricing = AWSPricingClientBuilder.standard().
			withCredentials( getCredentialsProvider() ).
			withRegion( Regions.fromName(getConfiguration().getRegion()) ).
			build();

		logInfo( LOG, "Getting started with AWS Pricing..." );

		String pricingInfo;
		String serviceCode = getConfiguration().getReportType().getSubtype();
		List<Filter> filterList = new ArrayList<Filter>();
		if (getConfiguration().hasPriceService()) {
			KeyValuePair[] filters = getConfiguration().getPriceFilters().toArray();
			if (filters != null) {
				for (int i = 0; i < filters.length; i++) {
					filterList.add(
						new Filter().
							withType( FilterType.TERM_MATCH ).
							withField( filters[i].getKey() ).
							withValue( filters[i].getValue() )
					);
				}
			}
		}
		GetProductsRequest request = new GetProductsRequest().
			withServiceCode( serviceCode ).
			withFilters( filterList ).
			withMaxResults( getConfiguration().getMaxResults() );
		GetProductsResult result;
		String nextToken = null;
		List<String> priceList = new ArrayList<String>();
		do {
			result = pricing.getProducts( request.withNextToken(nextToken) );
			priceList.addAll( result.getPriceList() );
			nextToken = result.getNextToken();
		}
		while (nextToken != null);
		result.getPriceList().clear();
		result.getPriceList().addAll( priceList );
		pricingInfo = format( result, getConfiguration().getDataFormat() );

		logInfo( LOG, String.format("Received pricing info:\n%s", pricingInfo) );

		return pricingInfo;
	}

	/**
	 * Formats and returns the given Billing and Cost Management API call
	 * result in the specified data format.
	 * 
	 * @return  the result of the API call in the specified format.
	 */
	@Override
	public String format( AmazonWebServiceResult<ResponseMetadata> result, DataFormat format )
	{
		JSONWriter writer = new JSONWriter();
		writer.startDocument();
		if (result instanceof GetProductsResult) {
			writer.addQuotedField( "FormatVersion", ((GetProductsResult)result).getFormatVersion() );
			writer.startList( "PriceList" );
			List<String> priceList = ((GetProductsResult)result).getPriceList();
			for (String product: priceList ) {
				writer.addElement( product );
			}
			writer.endList();
		}
		else {
			writer.addQuotedField( "Error", String.format("Unsupported result type - %s", result.getClass().getName()) );
		}
		writer.endDocument();

		return writer.toString();
	}


	private static final Log LOG = LogFactory.getLog( PriceImpl.class );
}
