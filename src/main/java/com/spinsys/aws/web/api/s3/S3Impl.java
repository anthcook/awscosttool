package com.spinsys.aws.web.api.s3;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.internal.SSEResultBase;
import com.spinsys.aws.web.api.AWSImpl;
import com.spinsys.aws.web.util.Parameters;


/**
 * Base implementation for AWS Billing and Cost Management API classes.
 */
public abstract class S3Impl
extends AWSImpl
{
	/**
	 * Formats and returns the given Simple Storage Service (S3) API call
	 * result in the specified data format.
	 * 
	 * @return  the result of the API call in the specified format.
	 */
	public abstract String format( SSEResultBase result, DataFormat format );


    protected S3Impl( Parameters config )
	throws AmazonClientException
    {
		super( config );
	}
}
