package com.spinsys.aws.web.api.billing;

import java.util.List;
import java.util.Map;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.AmazonWebServiceResult;
import com.amazonaws.ResponseMetadata;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.budgets.AWSBudgets;
import com.amazonaws.services.budgets.AWSBudgetsClientBuilder;
import com.amazonaws.services.budgets.model.Budget;
import com.amazonaws.services.budgets.model.CalculatedSpend;
import com.amazonaws.services.budgets.model.CostTypes;
import com.amazonaws.services.budgets.model.DescribeBudgetRequest;
import com.amazonaws.services.budgets.model.DescribeBudgetResult;
import com.amazonaws.services.budgets.model.DescribeBudgetsRequest;
import com.amazonaws.services.budgets.model.DescribeBudgetsResult;
import com.amazonaws.services.budgets.model.NotFoundException;
import com.amazonaws.services.budgets.model.Spend;
import com.spinsys.aws.web.util.Parameters;
import com.veetechis.lib.text.JSONWriter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Billing and Cost Management API implementation that provides access to
 * budgets defined on the account.
 */
public class BudgetImpl
extends BCMImpl
{
	/**
	 * Creates an instance of BudgetImpl.
	 */
	public BudgetImpl( Parameters configuration )
	{
		super( configuration );
	}

	
	/**
	 * Queries and returns information about a budget defined on the account.
	 * If a specific budget isn't specified by name in configuration then
	 * returns all currently defined budgets.
	 * 
	 * @return  budget information on the account.
	 * @see  AWSImpl#execute()
	 */
	@Override
	public String execute()
	throws AmazonClientException, AmazonServiceException
	{
		AWSBudgets budgets = AWSBudgetsClientBuilder.standard().
			withCredentials( getCredentialsProvider() ).
			withRegion( Regions.fromName(getConfiguration().getRegion()) ).
			build();

		logInfo( LOG, "Getting started with AWS Budgets..." );

		String budgetInfo;
		String budgetName = getConfiguration().getReportType().getSubtype();
		if (budgetName != null) {
			DescribeBudgetRequest request =
				new DescribeBudgetRequest().
					withAccountId( getConfiguration().getAccountID() ).
					withBudgetName( budgetName );
			DescribeBudgetResult result = null;
			try {
				result = budgets.describeBudget( request );
				budgetInfo = format( result, getConfiguration().getDataFormat() );
			}
			catch (NotFoundException e) {
				budgetInfo = "{\"Error\": \"" + e.getMessage() + "\"}";
			}
		}
		else {
			DescribeBudgetsRequest request =
				new DescribeBudgetsRequest().
					withAccountId( getConfiguration().getAccountID() ).
					withMaxResults( getConfiguration().getMaxResults() );
			DescribeBudgetsResult result = budgets.describeBudgets( request );
			budgetInfo = format( result, getConfiguration().getDataFormat() );
		}

		logInfo( LOG, String.format("Received budget info:\n%s", budgetInfo) );

		return budgetInfo;
	}

	/**
	 * Formats and returns the given Billing and Cost Management API call
	 * result in the specified data format.
	 * 
	 * @return  the result of the API call in the specified format.
	 */
	@Override
	public String format( AmazonWebServiceResult<ResponseMetadata> result, DataFormat format )
	{
		JSONWriter writer = new JSONWriter();

		writer.startDocument();
		if (result instanceof DescribeBudgetResult) {
			writer.startObject( "Budget" );
			formatBudget( ((DescribeBudgetResult)result).getBudget(), writer );
			writer.endObject();
		}
		else if (result instanceof DescribeBudgetsResult) {
			writer.startList( "Budgets" );
			List<Budget> budgetList = ((DescribeBudgetsResult)result).getBudgets();
			for (Budget budget: budgetList) {
				writer.startObject();
				formatBudget( budget, writer );
				writer.endObject();
			}
			String token = ((DescribeBudgetsResult)result).getNextToken();
			writer.endList();
			if (token != null) {
				writer.addStringField( "NextToken", ((DescribeBudgetsResult)result).getNextToken() );
			}
		}
		else {
			writer.addStringField( "Error", String.format("Unsupported result type - %s", result.getClass().getName()) );
		}
		writer.endDocument();

		return writer.toString();
	}

	protected void formatBudget( Budget budget, JSONWriter writer )
	{
		formatSpend( "BudgetLimit", budget.getBudgetLimit(), writer, true );
		writer.addQuotedField( "BudgetName", budget.getBudgetName() );
		writer.addQuotedField( "BudgetType", budget.getBudgetType() );
		formatCalculatedSpend( budget.getCalculatedSpend(), writer );
		formatCostFilters( budget.getCostFilters(), writer );
		formatCostTypes( budget.getCostTypes(), writer );
		writer.addUnquotedField( "LastUpdatedTime", String.valueOf(budget.getLastUpdatedTime().getTime()) );
		formatPlannedBudgetLimits( budget.getPlannedBudgetLimits(), writer );
		writer.startObject( "TimePeriod" );
		writer.addUnquotedField( "End", String.valueOf(budget.getTimePeriod().getEnd().getTime()) );
		writer.addUnquotedField( "Start", String.valueOf(budget.getTimePeriod().getStart().getTime()) );
		writer.endObject();
		writer.addQuotedField( "TimeUnit", budget.getTimeUnit() );
	}

	protected void formatSpend( String field, Spend spend, JSONWriter writer, boolean withNext )
	{
		if (spend != null) {
			writer.startObject( field );
			writer.addQuotedField( "Amount", spend.getAmount().toPlainString() );
			writer.addQuotedField( "Unit", spend.getUnit() );
			writer.endObject();
		}
	}

	protected void formatCalculatedSpend( CalculatedSpend calculatedSpend, JSONWriter writer )
	{
		if (calculatedSpend != null) {
			writer.startObject( "CalculatedSpend" );
			Spend aSpend = calculatedSpend.getActualSpend();
			Spend fSpend = calculatedSpend.getForecastedSpend();
			formatSpend( "ActualSpend", aSpend, writer, (fSpend != null) );
			formatSpend( "ForecastedSpend", fSpend, writer, false );
			writer.endObject();
		}
	}

	protected void formatCostFilters( Map<String,List<String>> costFilters, JSONWriter writer )
	{
		if (costFilters != null) {
			writer.startObject( "CostFilters" );
			int max = costFilters.size();
			while (max > 0) {
				StringBuilder value = new StringBuilder();
				for (String field : costFilters.keySet()) {
					value.append( "[" );
					for (int i = 0; i < costFilters.get(field).size(); i++) {
						if (i > 0) value.append( ", " );
						value.append( costFilters.get(field).get(i) );
					}
					value.append( "]" );
					writer.addQuotedField( field, value.toString() );
					value.delete( 0, value.length() );
				}
			}
			writer.endObject();
		}
	}

	protected void formatCostTypes( CostTypes costTypes, JSONWriter writer )
	{
		if (costTypes != null) {
			writer.startObject( "CostTypes" );
			writer.addUnquotedField( "IncludeCredit", String.valueOf(costTypes.isIncludeCredit()) );
			writer.addUnquotedField( "IncludeDiscount", String.valueOf(costTypes.isIncludeDiscount()) );
			writer.addUnquotedField( "IncludeOtherSubscription", String.valueOf(costTypes.isIncludeOtherSubscription()) );
			writer.addUnquotedField( "IncludeRecurring", String.valueOf(costTypes.isIncludeRecurring()) );
			writer.addUnquotedField( "IncludeRefund", String.valueOf(costTypes.isIncludeRefund()) );
			writer.addUnquotedField( "IncludeSubscription", String.valueOf(costTypes.isIncludeSubscription()) );
			writer.addUnquotedField( "IncludeSupport", String.valueOf(costTypes.isIncludeSupport()) );
			writer.addUnquotedField( "IncludeTax", String.valueOf(costTypes.isIncludeTax()) );
			writer.addUnquotedField( "IncludeUpfront", String.valueOf(costTypes.isIncludeUpfront()) );
			writer.addUnquotedField( "UseAmortized", String.valueOf(costTypes.isUseAmortized()) );
			writer.addUnquotedField( "UseBlended", String.valueOf(costTypes.isUseBlended()) );
			writer.endObject();
		}
	}

	protected void formatPlannedBudgetLimits( Map<String,Spend> budgetLimits, JSONWriter writer )
	{
		if (budgetLimits != null) {
			writer.startObject( "PlannedBudgetLimits" );
			int max = budgetLimits.size();
			for (String field : budgetLimits.keySet()) {
				formatSpend( field, budgetLimits.get(field), writer, (max-- > 1) );
			}
			writer.endObject();
		}
	}


	private static final Log LOG = LogFactory.getLog( BudgetImpl.class );
}
