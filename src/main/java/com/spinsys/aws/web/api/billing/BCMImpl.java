package com.spinsys.aws.web.api.billing;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonWebServiceResult;
import com.amazonaws.ResponseMetadata;
import com.spinsys.aws.web.api.AWSImpl;
import com.spinsys.aws.web.util.Parameters;


/**
 * Base implementation for AWS Billing and Cost Management API classes.
 */
public abstract class BCMImpl
extends AWSImpl
{
	/**
	 * Formats and returns the given Billing and Cost Management API call
	 * result in the specified data format.
	 * 
	 * @return  the result of the API call in the specified format.
	 */
	public abstract String format( AmazonWebServiceResult<ResponseMetadata> result, DataFormat format );

	
    protected BCMImpl( Parameters config )
	throws AmazonClientException
    {
		super( config );
	}
}
