package com.spinsys.aws.web.util;

import java.text.SimpleDateFormat;

public interface Constants
{
	public static enum DataFormat {
		JSON( "application/json" ),
		TEXT( "text/plain" ),
		TABLE( "text/csv" );
		
		public String getMimeType() { return mimeType; }
		
		private DataFormat( String mimeType ) {
			this.mimeType = mimeType;
		}
		
		private final String mimeType;
	}
	
	public static enum ReportType {
		BUDGET(), COST(), PRICE(), USAGE();
		
		public void setSubtype( String subtype )
		{
			this.subtype = subtype;
		}
		public String getSubtype()
		{
			return (!"ALL".equalsIgnoreCase(subtype) ? subtype : null);
		}

		public String toString()
		{
			StringBuilder out = new StringBuilder( normalize(name()) );
			if (subtype != null) {
				out.append( "_" ).append( subtype );
			}

			return out.toString();
		}

		private String normalize( String word )
		{
			return new StringBuilder( word.toUpperCase().substring(0, 1) ).
				append( word.toLowerCase().substring(1) ).toString();
		}

		private ReportType() {}

		private String subtype;
	}

	public static enum S3Action {
		DELETE, GET, LIST, PUT;
	}

	public static enum Parameter {
		CONFIG_FILE( "config", "--config" ),
		AWS_ACCOUNT_ID( "accountid", "--accountid" ),
		AWS_PROFILE( "profile", "--profile" ),
		AWS_ACCESS_KEY( "accesskey", "--accesskey" ),
		AWS_SECRET_KEY( "secretkey", "--secretkey" ),
		AWS_SESSION_TOKEN( "sessiontoken", "--sessiontoken" ),
		AWS_REGION( "region", "--region" ),
		OUTPUT_PATH( "output", "--output" ),
		REPORT_FORMAT( "format", "--format" ),
		REPORT_TYPE( "type", "--type" ),
		REPORT_SUBTYPE( "subtype", "--subtype" ),
		TIME_PERIOD( "period", "--period" ),
		GROUP_BY( COST_GROUP, "--groupby" ),
		FILTER_BY( PRICE_FILTER, "--filterby" ),
		MAX_RESULTS( "maxresults", "--maxresults" );

		private Parameter( String property, String argument )
		{
			this.property = property;
			this.argument = argument;
		}

		public String toString()
		{
			return new StringBuilder( name() ).append( "{" ).
				append( "property: " ).append( property ).append( ", " ).
				append( "argument: " ).append( argument ).append( "}" ).
				toString();
		}

		public final String property, argument;
	}

	public final static Integer RESULTS_MAXIMUM = Integer.valueOf( 100 );
	public final static String AWS_ACCESS_KEY_ID_PROPERTY = "aws.accessKeyId";
	public final static String AWS_SECRET_ACCESS_KEY_PROPERTY = "aws.secretKey";
	public final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat( "yyyy-MM-dd" );
	public final static SimpleDateFormat DATETIME_FORMAT = new SimpleDateFormat( "MM-dd-yyyy HH:mm:ss" );

	// Report Type specific configuration properties
	public final static String BUDGET_NAME = "budget.name";

	public final static String PRICE_SERVICE = "price.service";
	public final static String PRICE_FILTER = "price.filter";
	public final static String FILTER_BY_FIELD_TMPL = PRICE_FILTER + ".%s.field.%d";
	public final static String FILTER_BY_VALUE_TMPL = PRICE_FILTER + ".%s.value.%d";

	public final static String COST_METRICS = "cost.metrics";
	public final static String COST_GROUP = "cost.group";
	public final static String GROUP_BY_TYPE_TMPL = COST_GROUP + ".%s.type.%d";
	public final static String GROUP_BY_KEY_TMPL = COST_GROUP + ".%s.key.%d";

	// S3 upload properties
	public final static String S3_BUCKET_NAME = "s3.bucket.name";
	public final static String S3_OBJECT_NAME = "s3.object.name";
}
