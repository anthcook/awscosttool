package com.spinsys.aws.web.util;

import com.veetechis.lib.io.FileUtils;
import com.veetechis.lib.preferences.ArgumentsParser;
import com.veetechis.lib.util.KeyValueException;
import com.veetechis.lib.util.KeyValueList;
import com.veetechis.lib.util.KeyValuePair;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class Parameters
implements Constants
{
	/**
	 * Default configuration value indicates a value must be provided by the
	 * user.
	 */
	protected static final String USER_MUST_PROVIDE = "userMustProvide";

	/**
	 * Creates a new instance with the given arguments.
	 * 
	 * @param args  the arguments to parse.
	 * @throws Exception  if a parsing error occurs.
	 */
	public Parameters( String[] args )
	throws Exception
	{
		if (args.length > 0) {
			String outputPath = null;
			KeyValueList parms = new ArgumentsParser( ArgumentsParser.DEFAULT_SWITCH, "=" ).parse( args );
			if (parms.get(Parameter.CONFIG_FILE.argument) != null) {
				readConfig( parms.get(Parameter.CONFIG_FILE.argument).getValue() );
				outputPath = config.getProperty( "outputPath" );
			}
			if (parms.get(Parameter.AWS_ACCOUNT_ID.argument) != null) {
				accountId = parms.get( Parameter.AWS_ACCOUNT_ID.argument ).getValue();
			}
			if (parms.get(Parameter.AWS_PROFILE.argument) != null) {
				profile = parms.get( Parameter.AWS_PROFILE.argument ).getValue();
			}
			if (parms.get(Parameter.AWS_ACCESS_KEY.argument) != null) {
				accessKey = parms.get( Parameter.AWS_ACCESS_KEY.argument ).getValue();
			}
			if (parms.get(Parameter.AWS_SECRET_KEY.argument) != null) {
				secretKey = parms.get( Parameter.AWS_SECRET_KEY.argument ).getValue();
			}
			if (parms.get(Parameter.AWS_SESSION_TOKEN.argument) != null) {
				sessionToken = parms.get( Parameter.AWS_SESSION_TOKEN.argument ).getValue();
			}
			if (parms.get(Parameter.AWS_REGION.argument) != null) {
				region = parms.get( Parameter.AWS_REGION.argument ).getValue().toLowerCase();
			}
			if (parms.get(Parameter.OUTPUT_PATH.argument) != null) {
				outputPath = parms.get( Parameter.OUTPUT_PATH.argument ).getValue();
			}
			if (parms.get(Parameter.MAX_RESULTS.argument) != null) {
				setMaxResults( parms.get(Parameter.MAX_RESULTS.argument).getValue() );
			}
			if (parms.get(Parameter.REPORT_FORMAT.argument) != null) {
				dataFormat = DataFormat.valueOf( parms.get(Parameter.REPORT_FORMAT.argument).getValue().toUpperCase() );
			}
			if (parms.get(Parameter.REPORT_TYPE.argument) != null) {
				setReportType(
					parms.get(Parameter.REPORT_TYPE.argument).getValue(),
					parms.get(Parameter.REPORT_SUBTYPE.argument) );
			}
			if (parms.get(Parameter.TIME_PERIOD.argument) != null) {
				setTimePeriod( parms.get(Parameter.TIME_PERIOD.argument).getValue() );
			}

			if (parms.get(Parameter.GROUP_BY.argument) != null) {
				config.setProperty( Parameter.GROUP_BY.property, parms.get(Parameter.GROUP_BY.argument).getValue() );
			}
			if (parms.get(Parameter.FILTER_BY.argument) != null) {
				config.setProperty( Parameter.FILTER_BY.property, parms.get(Parameter.FILTER_BY.argument).getValue() );
			}
			
			if (outputPath != null) {
				if (outputPath.toLowerCase().startsWith("s3://")) {
					String pathName = outputPath.substring( 5 );
					String bucketName;
					String objectName = null;
					int idx = pathName.indexOf( "/" );
					if (idx > -1) {
						bucketName = pathName.substring( 0, idx );
						objectName = pathName.substring( idx+1 );
					}
					else {
						bucketName = pathName;
					}
					config.setProperty( S3_BUCKET_NAME, bucketName );
					if (objectName != null) {
						config.setProperty( S3_OBJECT_NAME, objectName );
					}
					responseFile = new File( System.getProperty("java.io.tmpdir") );
				}
				else {
					responseFile = new File( outputPath );
				}
			}
		}
	}

	public String getAccountID()
	{
		return accountId;
	}

	public String getAccessKey()
	{
		return accessKey;
	}

	public String getSecretKey()
	{
		return secretKey;
	}

	public String getSessionToken()
	{
		return sessionToken;
	}

	public String getSecurityProfile()
	{
		return profile;
	}

	public String getRegion()
	{
		return region;
	}

	public File getResponseFile()
	{
		return responseFile;
	}

	public void setResponseFile( File rfile )
	{
		responseFile = rfile;
	}

	public DataFormat getDataFormat()
	{
		return dataFormat;
	}

	public ReportType getReportType()
	{
		return reportType;
	}

	public boolean hasPriceService()
	{
		return (validate(config.getProperty(Parameter.FILTER_BY.property)) != null);
	}

	public KeyValueList getPriceFilters()
	{
		KeyValueList filters = new KeyValueList();

		String priceService = config.getProperty( Parameter.FILTER_BY.property );
		String type, value;
		for (int i = 1; i < Integer.MAX_VALUE; i++) {
			type = config.getProperty( String.format(FILTER_BY_FIELD_TMPL, priceService, i) );
			value  = config.getProperty( String.format(FILTER_BY_VALUE_TMPL, priceService, i) );
			if (validate(type) != null && validate(value) != null) {
				try {
					filters.add( new KeyValuePair(type, value) );
				}
				catch (KeyValueException e) {
					if (LOG.isWarnEnabled()) {
						LOG.warn( String.format(
							"Unexpected error while parsing price filter '%s'.", priceService), e );
					}
				}
			}
			else {
				break;
			}
		}

		return filters;
	}

	public boolean hasCostGroup()
	{
		return (validate(config.getProperty(Parameter.GROUP_BY.property)) != null);
	}

	public KeyValueList getCostGrouping()
	{
		KeyValueList grouping = new KeyValueList();

		String groupName = config.getProperty( Parameter.GROUP_BY.property );
		String type, key;
		for (int i = 1; i <= 2; i++) {
			type = config.getProperty( String.format(GROUP_BY_TYPE_TMPL, groupName, i) );
			key  = config.getProperty( String.format(GROUP_BY_KEY_TMPL, groupName, i) );
			if (validate(type) != null && validate(key) != null) {
				try {
					grouping.add( new KeyValuePair(type, key) );
				}
				catch (KeyValueException e) {
					if (LOG.isWarnEnabled()) {
						LOG.warn( String.format(
							"Unexpected error while parsing cost group '%s'.", groupName), e );
					}
				}
			}
		}

		return grouping;
	}

	public boolean hasTimePeriod()
	{
		return (getTimePeriodStart() != null && getTimePeriodEnd() != null);
	}

	public Date getTimePeriodStart()
	{
		return startDate;
	}
	
	public Date getTimePeriodEnd()
	{
		return endDate;
	}
	
	public Integer getMaxResults()
	{
		if ((maxResults != null) && (maxResults < 1 || maxResults > 100)) {
			if (LOG.isWarnEnabled()) {
				LOG.warn( String.format("Unsupported max results value '%s'; default is being used.", maxResults) );
			}
			maxResults = null;
		}

		return (maxResults != null ? maxResults : RESULTS_MAXIMUM);
	}

	public boolean doUploadToS3()
	{
		return (getS3BucketName() != null);
	}

	public String getS3BucketName()
	{
		return config.getProperty( S3_BUCKET_NAME );
	}

	public String getS3ObjectName()
	{
		return config.getProperty( S3_OBJECT_NAME );
	}

	public void setS3ObjectName( String name )
	{
		config.setProperty( S3_OBJECT_NAME, name );
	}

	public boolean hasCredentials()
	{
		boolean hasCreds = hasConfigCredentials();
		if (!hasCreds) {
			hasCreds = (new File(System.getProperty("user.home"), ".aws/credentials")).exists();
		}

		return hasCreds;
	}
	
	public boolean hasSessionToken()
	{
		return (sessionToken != null);
	}
	
	public boolean hasConfigCredentials()
	{
		return (accessKey != null && secretKey != null);
	}
	
	public boolean hasRequiredParameters()
	{
		return (accountId != null && region != null && dataFormat != null && reportType != null);
	}

	public String getUsage()
	{
		return "Usage parameters:\n" +
				"\t--config=<configuration properties file>\n" +
				"\t--profile=<AWS security profile>\n" +
				"\t--accountid=<account ID to access>\n" +
				"\t--accesskey=<account access key ID>\n" +
				"\t--secretkey=<account secret access key>\n" +
				"\t--sessiontoken=<temporary session token>\n" +
				"\t--region=<account region [e.g. us-east-1]>\n" +
				"\t--output=<path to write response>\n" +
				"\t--format=<response format type [e.g. json|table|text]>\n" +
				"\t--type=<the report type [1. budget|2. cost|3. price|4. usage]>\n" +
				"\t--subtype=<the report data type [see README]>\n" +
				"\t--period=<start-date:end-date [yyyy-mm-dd]>\n" +
				"\t--groupby=<user defined cost group [see README]>\n" +
				"\t--filterby=<user defined price filter [see README]>\n" +
				"\t--maxresults=<the maximum number of results to return (1-100)>\n\n" +
				"** An output path can be a named file or a directory. If a directory then\n" +
				"   the file name used is the report type_subtype with suffix .rpt [e.g.\n" +
				"   Price_AmazonEC2.rpt]. If an output path isn't specified then the report\n" +
				"   is displayed on the console.\n\n" +
				"   The supported report types include:\n" +
				"\t1. AWS Budgets - list of current budgets on the account and their\n" +
				"\t       performance histories.\n" +
				"\t2. AWS Cost Explorer - current cost and usage metrics and forcasts.\n" +
				"\t3. AWS Price List - pricing information for specified services.\n" +
				"\t4. AWS Cost and Usage Reports - list and retrieve available cost and\n" +
				"\t       usage reports.\n";
	}
	
	public String toString()
	{
		StringBuilder out = new StringBuilder( "[" ).
			append( "Account ID = " ).append( getAccountID() ).append( "; " ).
			append( "Access Key = " ).append( getAccessKey() ).append( "; " ).
			append( "Secret Key = " ).append( getSecretKey() ).append( "; " ).
			append( "AWS Region = " ).append( getRegion() ).append( "; " ).
			append( "Report Type = " ).append( getReportType() ).append( "; " ).
			append( "Report Subtype = " ).append( getReportType().getSubtype() ).append( "; " ).
			append( "Report Format = " ).append( getDataFormat() ).append( "; " ).
			append( "Output Path = " ).append( getResponseFile() ).append( "; " ).
			append( "Max Results = " ).append( getMaxResults() ).
			append( "]" );

		return out.toString();
	}


	/**
	 * Validates the given argument value. Returns the value if validated,
	 * otherwise returns null.
	 * 
	 * @param argValue  the value to validate.
	 * @return  the validated value or null.
	 */
	protected String validate( String argValue )
	{
		String rval = argValue;

		if (USER_MUST_PROVIDE.equalsIgnoreCase(argValue)) {
			rval = null;
		}

		return rval;
	}


	private void setReportType( String type, KeyValuePair subtype )
	{
		reportType = ReportType.valueOf( type.toUpperCase() );
		setReportTypeConfig( reportType, config );
		if (subtype != null) {
			reportType.setSubtype( subtype.getValue() );
			switch (reportType) {
				case BUDGET:
					config.setProperty( BUDGET_NAME, subtype.getValue() );
					break;
				case COST:
					config.setProperty( COST_METRICS, subtype.getValue() );
					break;
				case PRICE:
					config.setProperty( PRICE_SERVICE, subtype.getValue() );
					break;
				default:
					// ignored
			}
		}
	}

	private void setTimePeriod( String timePeriod )
	{
		if (validate(timePeriod) != null) {
			String[] dates = timePeriod.split( "\\:" );
			if (dates.length == 2) {
				Calendar cal = Calendar.getInstance();
				String[] ymd;
				for (int i = 0; i < 2; i++) {
					ymd = dates[i].split( "\\-" );
					if (ymd.length == 3) {
						try {
							cal.set( Integer.parseInt(ymd[0]), Integer.parseInt(ymd[1])-1, Integer.parseInt(ymd[2]) );
							if (startDate == null) {
								startDate = cal.getTime();
							}
							else {
								endDate = cal.getTime();
							}
						}
						catch (ArrayIndexOutOfBoundsException e) {
							if (LOG.isErrorEnabled()) {
								LOG.error( String.format("Invalid calendar date specified: '%s'", dates[i]), e );
							}
						}
					}
				}
			}
			if (startDate == null || endDate == null) {
				if (LOG.isWarnEnabled()) {
					LOG.warn( String.format("Invalid time period specified, input ignored: '%s'.", timePeriod) );
				}
				startDate = null;
				endDate = null;
			}
		}
	}

	private void setMaxResults( String maximum )
	{
		if (validate(maximum) != null) {
			try {
				maxResults = Integer.valueOf( maximum );
			}
			catch (NumberFormatException e) {
				if (LOG.isErrorEnabled()) {
					LOG.error( String.format("Invalid value for property '%s': %s",
						Parameter.MAX_RESULTS.property, maximum) );
				}
			}
		}
	}

	private void readConfig( String configPath )
	throws IOException
	{
		File pfile = new File( configPath );
		config = new Properties();
		try {
			if (FileUtils.isReadableFile(pfile)) {
				config.load( new FileInputStream(pfile) );
				accountId = validate( config.getProperty(Parameter.AWS_ACCOUNT_ID.property) );
				accessKey = validate( config.getProperty(Parameter.AWS_ACCESS_KEY.property) );
				secretKey = validate( config.getProperty(Parameter.AWS_SECRET_KEY.property) );
				sessionToken = validate( config.getProperty(Parameter.AWS_SESSION_TOKEN.property) );
				profile = validate( config.getProperty(Parameter.AWS_PROFILE.property) );
				region = validate( config.getProperty(Parameter.AWS_REGION.property) );
				if (validate(config.getProperty(Parameter.REPORT_FORMAT.property)) != null) {
					dataFormat = DataFormat.valueOf(
						config.getProperty(Parameter.REPORT_FORMAT.property).toUpperCase() );
				}
				if (validate(config.getProperty(Parameter.REPORT_TYPE.property)) != null) {
					reportType = ReportType.valueOf(
						config.getProperty(Parameter.REPORT_TYPE.property).toUpperCase() );
					setReportTypeConfig( reportType, config );
					if (validate(config.getProperty(Parameter.REPORT_SUBTYPE.property)) != null) {
						reportType.setSubtype( config.getProperty(Parameter.REPORT_SUBTYPE.property) );
					}
				}
				setTimePeriod( config.getProperty(Parameter.TIME_PERIOD.property) );
				setMaxResults( config.getProperty(Parameter.MAX_RESULTS.property) );
				String outputPath = validate( config.getProperty(Parameter.OUTPUT_PATH.property) );
				if (outputPath != null) {
					config.setProperty( "outputPath", outputPath );
				}
			}
			else {
				if (LOG.isWarnEnabled()) {
					LOG.warn( String.format("Parameters file '%s' not readable, input ignored.", pfile) );
				}
			}
		}
		catch (FileNotFoundException e) {
			if (LOG.isWarnEnabled()) {
				LOG.warn( String.format("Parameters file '%s' not found, input ignored.", pfile) );
			}
		}
	}

	private void setReportTypeConfig( ReportType reportType, Properties config )
	{
		switch (reportType) {
			case BUDGET:
				if (validate(config.getProperty(BUDGET_NAME)) != null) {
					reportType.setSubtype( config.getProperty(BUDGET_NAME) );
				}
				break;
			case COST:
				if (validate(config.getProperty(COST_METRICS)) != null) {
					reportType.setSubtype( config.getProperty(COST_METRICS) );
				}
				break;
			case PRICE:
				if (validate(config.getProperty(PRICE_SERVICE)) != null) {
					reportType.setSubtype( config.getProperty(PRICE_SERVICE) );
				}
				break;
			default:
				// shouldn't get here...
				if (LOG.isErrorEnabled()) {
					LOG.error( String.format("Unknown report type '%s'.", reportType.toString()) );
				}
		}
	}


	private Integer maxResults;
	private String accountId;
	private String profile;
	private String accessKey;
	private String secretKey;
	private String sessionToken;
	private String region;
	private Date startDate;
	private Date endDate;
	private File responseFile;
	private Properties config;
	private DataFormat dataFormat;
	private ReportType reportType;

	private static final Log LOG = LogFactory.getLog( Parameters.class );
}
