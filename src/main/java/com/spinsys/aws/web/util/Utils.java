package com.spinsys.aws.web.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class Utils
{
	/**
	 * Returns the given JSON document in pretty print format. Returns the
	 * document in original form if an error occurs and logs the error.
	 * 
	 * @param  json
	 * @return  pretty print version of the document, or original in event of
	 *            an error.
	 */
	public static String beautify( String json )
	{
		String rjson = json;

		ObjectMapper mapper = new ObjectMapper();

		try {
			Object obj = mapper.readValue( json, Object.class );
			rjson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString( obj );
		}
		catch (JsonProcessingException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error( String.format("Error processing JSON document: %s", json), e );
			}
		}

		return rjson;
	}


	private static final Log LOG = LogFactory.getLog( Utils.class );
}
