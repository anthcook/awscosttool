#!/bin/bash
COMMANDJAR=${COMMANDJAR:-"aws.cost.tool-0.1.1.jar"}
AWSPROFILE=${AWSPROFILE:-"default"}
REPORTSDIR=${REPORTSDIR:-"./"}
REPORTTIME=${REPORTTIME:-""}
CONFIGFILE=${CONFIGFILE:-""}
EXECHELP=false

function parse_period() {
	SDATE=$(echo $1 | cut -d ":" -f1)
	EDATE=$(echo $1 | cut -d ":" -f2)
	START=$(echo $SDATE | cut -d "-" -f1)$(echo $SDATE | cut -d "-" -f2)$(echo $SDATE | cut -d "-" -f3)
	END=$(echo $EDATE | cut -d "-" -f1)$(echo $EDATE | cut -d "-" -f2)$(echo $EDATE | cut -d "-" -f3)
	echo ${START}-${END}
}

OPTERR=0
while getopts "c:d:j:p:t:h" PARM
do
	case "$PARM" in
		"c") CONFIGFILE=${OPTARG}
			;;
		"d") REPORTSDIR=${OPTARG}
			;;
		"j") COMMANDJAR=${OPTARG}
			;;
		"p") AWSPROFILE=${OPTARG}
			;;
		"t") REPORTTIME=${OPTARG}
			;;
		"h") EXECHELP=true
			;;
	esac
done

OUTPUTDIR="--output=${REPORTSDIR}"
PROFILE="--profile=${AWSPROFILE}"
if test "${REPORTTIME}." != "."
then
	PERIOD="--period=${REPORTTIME}"
	OUTPUTSFX=$(parse_period ${REPORTTIME})
else
	PERIOD=""
	OUTPUTSFX=$(date +%m%d)
fi
if test "${CONFIGFILE}." != "."
then
	CONFIG="--config=${CONFIGFILE}"
else
	CONFIG=""
fi

LOGFILE=./get-costs.log
if test -f ${LOGFILE}
then
	rm ${LOGFILE}
fi

if test $EXECHELP = true
then
	#java -jar ${COMMANDJAR}
	echo "$0 - Pull AWS Cost Explorer reports for MDACA Cost Control Analysis"
	echo "Usage:"
	echo "  -c <config file>"
	echo "     Path to MDACA AWS Cost Tool configuration properties file."
	echo "  -d <output directory>"
	echo "     Path to directory for CCA reports (default = current directory)."
	echo "  -h"
	echo "     Display this message."
	echo "  -p <AWS credentials profile>"
	echo "     (default = 'default')"
	echo "  -t <report time period>"
	echo "     Format = yyyy-mm-dd:yyyy-mm-dd [start-end] (default = current month)"
else
	REPORTGROUPS="InstanceTypeInstanceId ProjectInstanceId ProjectInstanceName ProjectLinked ProjectService ProjectUsageType"
	for GROUP in $REPORTGROUPS
	do
		REPORTFILE=Costs-${GROUP}-${OUTPUTSFX}.json
		echo "Getting costs report for group '${GROUP}' to file '${REPORTSDIR}/${REPORTFILE}'"
		java -jar ${COMMANDJAR} ${CONFIG} ${PROFILE} ${PERIOD} ${OUTPUTDIR}/${REPORTFILE} --groupby=${GROUP} --type=cost >> ${LOGFILE}
	done
fi
