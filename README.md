# README #

### What is this repository for? ###

A Java tool for querying AWS Billing and Cost Management APIs.

### How do I use it? (CLI) ###

* Clone this repository to your local workstation, change directory to
./awscosttool and build it with `mvn clean package -DskipTests`. Copy the
resulting JAR file in .../target to a suitable work location and run the
jar without arguments to see usage parameters, e.g.:
####
```
$ java -jar aws.cost.tool-0.0.1-SNAPSHOT.jar
Usage parameters:
        --config=<configuration properties file>
        --profile=<AWS security profile>
        --accountid=<account ID to access>
        --accesskey=<account access key ID>
        --secretkey=<account secret access key>
        --region=<account region [e.g. us-east-1]>
        --output=<path to write response>
        --format=<response format type [e.g. json|table|text]>
        --type=<the report type [1. budget|2. cost|3. price|4. usage]>
        --subtype=<the report data type [see README]>
        --period=<start-date:end-date [yyyy-mm-dd]>
        --groupby=<user defined cost group [see README]>
        --filterby=<user defined price filter [see README]>
        --maxresults=<the maximum number of results to return (1-100)>

** An output path can be a named file or a directory. If a directory then
   the file name used is the report type_subtype with suffix .rpt [e.g.
   Price_AmazonEC2.rpt]. If an output path isn't specified then the report
   is displayed on the console.

   The supported report types include:
        1. AWS Budgets - list of current budgets on the account and their
               performance histories.
        2. AWS Cost Explorer - current cost and usage metrics and forcasts.
        3. AWS Price List - pricing information for specified services.
        4. AWS Cost and Usage Reports - list and retrieve available cost and
               usage reports.
```

#### Configuration ####
(*TODO*)

* Create a properties file for frequently used parameters (e.g. accountid,
accesskey, secretkey, region), and use --config to specify their values without
having to explicitly pass them on the CLI, e.g.:

*myparms.cfg*
```
accountid=123456789012
#accesskey=<use ~/.aws/credentials>
#secretkey=<use ~/.aws/credentials>
region=us-east-1
format=json
```

`$ java -jar aws.cost.tool-0.0.1-SNAPSHOT.jar --config=myparms.cfg
--type=budget # get list of budgets on the account`

See _config.properties_ for property details.

### How do I develop with it? (API) ###
(*TODO*)

See _com.spinsys.aws.web.CostToolMain_ for coding example.
